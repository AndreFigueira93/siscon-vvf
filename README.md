# SISCON-VVF | Sistema de Conselhos em Vue - Vuetify - Firebase

> Projeto base desenvolvido para o Conselho da Amapá Previdência

## Passo a passo para rodar

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```