// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import { store } from './store'
import DateFilter from './filters/date'
import AlertCmp from './components/Globais/Alertas.vue'

Vue.use(Vuetify)

Vue.config.productionTip = false

Vue.filter('date', DateFilter)
Vue.component('app-alert', AlertCmp)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    if (localStorage.getItem('uid')) {
      this.$store.dispatch('setUser', localStorage.getItem('uid'))
    }
    firebase.initializeApp({
      apiKey: 'AIzaSyC0_Dfk_DsEzlMGSnLhHS8jXzGwM1MHSVk',
      authDomain: 'amapa-previdencia.firebaseapp.com',
      databaseURL: 'https://amapa-previdencia.firebaseio.com',
      projectId: 'amapa-previdencia',
      storageBucket: 'amapa-previdencia.appspot.com',
      messagingSenderId: '572729656754'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadReunions')
  }
})
