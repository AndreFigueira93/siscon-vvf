import Vue from 'vue'
import Router from 'vue-router'
import Inicio from '@/components/Inicio'
import CriarReuniao from '@/components/Reuniao/CriarReuniao'
import Reunioes from '@/components/Reuniao/Reunioes'
import Reuniao from '@/components/Reuniao/Reuniao'
import CriarUsuario from '@/components/Usuario/CriarUsuario'
import Perfil from '@/components/Usuario/Perfil'
import Entrar from '@/components/Usuario/Entrar'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Início',
      component: Inicio
    },
    {
      path: '/reunioes',
      name: 'Reuniões',
      component: Reunioes,
      beforeEnter: AuthGuard
    },
    {
      path: '/reuniao/:id',
      name: 'Reunião',
      props: true,
      component: Reuniao,
      beforeEnter: AuthGuard
    },
    {
      path: '/perfil',
      name: 'Perfil',
      component: Perfil,
      beforeEnter: AuthGuard
    },
    {
      path: '/entrar',
      name: 'Entrar',
      component: Entrar
    },
    {
      path: '/criar-reuniao',
      name: 'Criar Reunião',
      component: CriarReuniao,
      beforeEnter: AuthGuard
    },
    {
      path: '/criar-usuario',
      name: 'Criar Usuário',
      component: CriarUsuario,
      beforeEnter: AuthGuard
    }
  ],
  mode: 'history'
})
