import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    loadedReunions: [],
    user: null,
    loading: false,
    error: null
  },
  mutations: {
    criarReuniao (state, payload) {
      state.loadedReunions.push(payload)
    },
    setUser (state, payload) {
      state.user = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    clearError (state) {
      state.error = null
    },
    setLoadedReunions (state, payload) {
      state.loadedReunions = payload
    }
  },
  actions: {
    loadReunions ({commit}) {
      commit('setLoading', true)
      firebase.database().ref('reunioes').on('value', function (data) {
        const reunions = []
        const obj = data.val()
        for (let key in obj) {
          reunions.push({
            id: key,
            title: obj[key].title,
            description: obj[key].description,
            imageUrl: obj[key].imageUrl,
            date: obj[key].date
          })
        }
        commit('setLoadedReunions', reunions)
        commit('setLoading', false)
      })
        /*  .then((data) => {
          const reunions = []
          const obj = data.val()
          for (let key in obj) {
            reunions.push({
              id: key,
              title: obj[key].title,
              description: obj[key].description,
              imageUrl: obj[key].imageUrl,
              date: obj[key].date
            })
          }
          commit('setLoadedReunions', reunions)
        })
        .catch(
          (error) => {
            console.log(error)
          }
        )
        */
    },
    criarReuniao ({commit}, payload) {
      const reuniao = {
        title: payload.title,
        location: payload.location,
        imageUrl: payload.imageUrl,
        description: payload.description,
        date: payload.date
      }
      firebase.database().ref('reunioes').push(reuniao)
      .then((data) => {
        const key = data.key
        commit('criarReuniao', {
          ...reuniao,
          id: key
        })
      })
      .catch((error) => {
        console.log(error)
      })
    },
    signUserUp ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              id: user.uid,
              registeredReunions: []
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
            console.log(error)
          }
        )
    },
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
              id: user.uid,
              registeredReunions: []
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit('setError', error)
            console.log(error)
          }
        )
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {id: payload.uid, registeredReunions: []})
    },
    logout ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
    },
    clearError ({commit}) {
      commit('clearError')
    }
  },
  getters: {
    loadedReunions (state) {
      return state.loadedReunions.sort((reunionA, reunionB) => {
        return reunionA.date > reunionB.date
      })
    },
    featuredReunions (state, getters) {
      return getters.loadedReunions.slice(0, 5)
    },
    loadedReunion (state) {
      return (reunionId) => {
        return state.loadedReunions.find((reunion) => {
          return reunion.id === reunionId
        })
      }
    },
    user (state) {
      return state.user
    },
    loading (state) {
      return state.loading
    },
    error (state) {
      return state.error
    }
  }
})
